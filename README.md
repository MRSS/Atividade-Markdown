##  Mateus Romano Saback Santos <br>
### **Ano de nascimento:** 2002 (o único ano palíndromo do século) <br> **Telefone:** +55 (71) 71717171 (infelizmente, não há garantia que este número de telefone é real) <br> **E-mail:** mateusyasd@hotmail.com <br> **Endereço:** Terra, Sistema Solar, Universo <br>

----

> #### **Objetivo:**
> ##### <br> - Ser aprovado no processo seletivo da InfoJr;
> ##### - Aprender a programar (de verdade).

<br>

> #### **Experiência:** 
> ##### <br> - Criou ***4*** jogos no RPG Maker <br> ([Ver página no GameJolt](https://gamejolt.com/@mateusparagames/games));
> ##### - Utilizou o software RPG Maker MV por mais de 500 horas ([de acordo com o seu perfil no Steam](https://steamcommunity.com/profiles/76561198315821433)) e o software RPG Maker VX Ace por mais de 1000 horas (pode provar depois, se for necessário);
> ##### - Pussui experiência extremamente básica com a Linguagens Ruby e Javascript devido a alteraçoes feitas no código-fonte pré-existente utilizado em seus jogos de RPG Maker;
> ##### - Fã de Pokémon, acumulou mais de 300 horas de jogo nos jogos de Pokemon do 3DS;
> ##### - Zerou 3 jogos de Pokémon diferentes (Fire Red, X e Ultra Moon);
> ##### - Completou 100% o modo história dos jogos New Super Mario Bros. 2 e New Super Mario Bros. U (obteve perfil com 5 estrelas);
> ##### - Derrotou Sans no jogo UNDERTALE.